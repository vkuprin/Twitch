import localesEnum from './localesEnum';
import enRoutes from './routes/enRoutes.json';
import ruRoutes from './routes/ruRoutes.json';
import enPages from './pages/enPages.json';
import ruPages from './pages/ruPages.json';
import enHeader from './header/enHeader.json';
import ruHeader from './header/ruHeader.json';

const languageToLocale = (language) => {
    if (language.split(`-`)[0] === `ru`) {
        return localesEnum.ru;
    }
    return localesEnum.en;
};


const localesService = {
    getCurrentLocale: () => {
        const { pathname } = window.location;
        const { search } = window.location;
        const pathParts = pathname.split(`/`);
        const language = pathParts[1];
        if (!Object.values(localesEnum).includes(language)) {
            const userLanguage = navigator.language || navigator.userLanguage;
            const userLocale = languageToLocale(userLanguage);

            window.location = `/${userLocale}${pathname}${search}`;
        }
        return language;
    },

    getRouteLocale: (locale) => {
        const routesLocale = { ...enRoutes };

        if (locale === localesEnum.ru) {
            Object.entries(ruRoutes).forEach(([key, item]) => {
                routesLocale[key] = { ...routesLocale[key], ...item };
            });
        }

        return routesLocale;
    },
    getPageLocale: (locale) => {
        const pagesLocale = { ...enPages };

        if (locale === localesEnum.ru) {
            Object.entries(ruPages).forEach(([key, item]) => {
                pagesLocale[key] = { ...pagesLocale[key], ...item };
            });
        }

        return pagesLocale;
    },
    getHeaderLocale: (locale) => {
        const headerLocale = { ...enHeader };

        if (locale === localesEnum.ru) {
            Object.entries(ruHeader).forEach(([key, item]) => {
                headerLocale[key] = { ...headerLocale[key], ...item };
            });
        }

        return headerLocale;
    }
};


export default localesService;
