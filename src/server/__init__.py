from .. import static


def get_dir():
    import os
    return os.path.dirname(static.__file__)
