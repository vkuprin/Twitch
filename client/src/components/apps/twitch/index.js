import '@babel/polyfill';

import React, { Fragment } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Container } from 'react-bootstrap';
import MetaPage from './components/MetaPage';
import GoogleAnalytics from './components/GoogleAnalytics';
import GlobalHeader from './components/GlobalHeader/GlobalHeader';
import routes from './routes/routes';
import localesService from './locales/localesService';
import GlobalLoaderV2 from '../../../../../FrontendCore/Components/GlobalLoaderV2/GlobalLoaderV2';
import GlobalAlert from '../../../../../FrontendCore/Components/GlobalAlert/GlobalAlert';
import globalAlertWrappers from '../../../../../FrontendCore/Components/GlobalAlert/globalAlertWrappers';

const currentLocale = localesService.getCurrentLocale();
const routeLocale = localesService.getRouteLocale(currentLocale);

const routeComponents = Object.entries(routes).map(([key, item]) => (
    <Route
        key={item.uid}
        exact={item.exact}
        path={routeLocale[key].path}
        render={routeProps => (
            <Fragment>
                <MetaPage
                    title={routeLocale[key].title}
                    description={routeLocale[key].description}
                    robots={item.robots}
                    {...routeProps}
                />
                <item.component {...routeProps} />
            </Fragment>
        )}
    />
));

ReactDOM.render(
    <Router>
        <GoogleAnalytics />
        <GlobalLoaderV2 />
        <GlobalHeader />
        <GlobalAlert Wrapper={globalAlertWrappers.ContainerWrapper} />
        <Container>
            <Switch>
                {routeComponents}
            </Switch>
        </Container>
    </Router>,
    document.getElementById(`root-js`)
);
