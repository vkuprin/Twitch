import React, { Fragment } from 'react';
import { observer } from 'mobx-react';
import { Button } from 'react-bootstrap';
import {
    globalAlertAdapter,
    globalAlertVariants
} from '../../../../../../../FrontendCore/Components/globalAlert/globalAlertAdapter';

@observer
class Twitch extends React.Component {
    constructor(props) {
        super(props);

        this.state = {};
    }

    render() {
        return (
            <Fragment>
                <Button onClick={() => globalAlertAdapter.addAlert({
                    variant: globalAlertVariants.primary,
                    title: `Title`,
                    message: `Message`
                })}
                >
                    add some
                </Button>
            </Fragment>
        );
    }
}


export default Twitch;
