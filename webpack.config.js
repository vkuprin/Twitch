const config = require(`./FrontendCore/webpack.base`);
const { entry } = require(`./src/static/webpack.extend`);

module.exports = { ...config, entry };
