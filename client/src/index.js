import '@babel/polyfill';

import * as serviceWorker from './serviceWorker';

import React, { Fragment } from 'react';
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom';

import { Container } from 'react-bootstrap';
import GlobalAlert from '../../FrontendCore/Components/GlobalAlert/GlobalAlert';
import GlobalHeader from '../src/components/apps/twitch/components/GlobalHeader/GlobalHeader';
import GlobalLoaderV2 from '../../FrontendCore/Components/GlobalLoaderV2/GlobalLoaderV2';
import GoogleAnalytics from '../src/components/apps/twitch/components/GoogleAnalytics'

import MetaPage from '../src/components/apps/twitch/components/MetaPage';
import ReactDOM from 'react-dom';
import globalAlertWrappers from '../../../FrontendCore/Components/GlobalAlert/globalAlertWrappers';
import localesService from '../src/components/apps/twitch/locales/localesService'
import routes from '../src/components/apps/twitch/routes/routes';

// import Index from './components/apps/twitch/index';
// import React from 'react';
// import ReactDOM from 'react-dom';

// ReactDOM.render(<Index />, document.getElementById('root'));



const currentLocale = localesService.getCurrentLocale();
const routeLocale = localesService.getRouteLocale(currentLocale);

const routeComponents = Object.entries(routes).map(([key, item]) => (
    <Route
        key={item.uid}
        exact={item.exact}
        path={routeLocale[key].path}
        render={routeProps => (
            <Fragment>
                <MetaPage
                    title={routeLocale[key].title}
                    description={routeLocale[key].description}
                    robots={item.robots}
                    {...routeProps}
                />
                <item.component {...routeProps} />
            </Fragment>
        )}
    />
));

ReactDOM.render(
    <Router>
        <GoogleAnalytics />
        <GlobalLoaderV2 />
        <GlobalHeader />
        <GlobalAlert Wrapper={globalAlertWrappers.ContainerWrapper} />
        <Container>
            <Switch>
                {routeComponents}
            </Switch>
        </Container>
    </Router>,
    document.getElementById(`root`) //
);


serviceWorker.unregister();
