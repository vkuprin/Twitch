/* eslint-disable react/prop-types */
import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import {
    Button, Col, Image, Modal, Row
} from 'react-bootstrap';
import JSONPretty from 'react-json-pretty';
import cn from 'classnames';
import styles from './styles.less';


class FollowRow extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isModalOpen: false
        };
    }

    modalClose = () => {
        this.setState({ isModalOpen: false });
    };
    modalOpen = () => {
        this.setState({ isModalOpen: true });
    };

    render() {
        const { isModalOpen } = this.state;
        const { follow } = this.props;
        const { created_at, channel, notifications } = follow;

        return (
            <Fragment>
                <Row className={styles.cardSpace}>
                    <Col xs={2}>
                        <Image
                            className={styles.avatar}
                            src={channel.logo}
                            thumbnail
                            alt={channel.display_name}
                        />
                    </Col>
                    <Col xs={8} className={styles.verticalCenter}>
                        <a
                            className={cn(styles.whiteLink)}
                            href={`https://www.twitch.tv/${channel.name}`}
                            target="_blank"
                            rel="noopener noreferrer"
                        >
                            <h5>{channel.display_name}</h5>
                        </a>
                    </Col>
                    <Col xs={2}>
                        <Button variant="primary" onClick={this.modalOpen}>Info</Button>
                    </Col>
                </Row>

                <Modal show={isModalOpen} onHide={this.modalClose}>
                    <Modal.Header closeButton>
                        <Modal.Title>{channel.display_name}</Modal.Title>
                    </Modal.Header>

                    <Modal.Body>
                        <JSONPretty data={follow} />
                    </Modal.Body>

                    <Modal.Footer>
                        <Button variant="secondary" onClick={this.modalClose}>Close</Button>
                    </Modal.Footer>
                </Modal>
            </Fragment>
        );
    }
}

FollowRow.propTypes = {
    follow: PropTypes.shape({
        created_at: PropTypes.string,
        channel: PropTypes.shape({
            mature: PropTypes.bool,
            status: PropTypes.string,
            broadcaster_language: PropTypes.string,
            broadcaster_software: PropTypes.string,
            display_name: PropTypes.string,
            game: PropTypes.string,
            language: PropTypes.string,
            id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
            name: PropTypes.string,
            created_at: PropTypes.string,
            updated_at: PropTypes.string,
            partner: PropTypes.bool,
            logo: PropTypes.string,
            video_banner: PropTypes.string,
            profile_banner: PropTypes.string,
            profile_banner_background_color: PropTypes.string,
            url: PropTypes.string,
            views: PropTypes.number,
            followers: PropTypes.number,
            broadcaster_type: PropTypes.string,
            description: PropTypes.string,
            private_video: PropTypes.bool,
            privacy_options_enabled: PropTypes.bool
        }).isRequired,
        notifications: PropTypes.bool
    }).isRequired
};

export default FollowRow;
