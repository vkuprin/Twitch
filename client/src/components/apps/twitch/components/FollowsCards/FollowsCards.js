import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Col, Row } from 'react-bootstrap';
import FollowCard from './FollowCard/FollowCard';


class FollowsCards extends React.Component {
    constructor(props) {
        super(props);

        this.state = {};
    }

    render() {
        const { follows } = this.props;
        if (!follows) {
            return null;
        }

        const usersCards = follows.map(item => (
            <Col key={item.channel.id} sx={12} sm={12} md={6} lg={4} xl={3}>
                <FollowCard key={item.channel.id} follow={item} />
            </Col>
        ));

        return (
            <Fragment>
                <Row>
                    {usersCards}
                </Row>
            </Fragment>
        );
    }
}

FollowsCards.propTypes = {
    follows: PropTypes.arrayOf(PropTypes.object)
};


export default FollowsCards;
