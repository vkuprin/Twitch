import logging
import os

import tornado.escape
import tornado.ioloop
import tornado.web
import tornado.websocket

from Submodules.Defence.cookie_secret import cookie_secret
from pypattyrn.creational.singleton import Singleton
from src import handlers
from .. import static


class PyServer(metaclass=Singleton):
    @staticmethod
    def make_app():
        settings = {
            "static_path": os.path.dirname(os.path.abspath(static.__file__)),
            "static_url_prefix": "/static/"
        }
        return tornado.web.Application([
            (r"/api/Users/TranslateUsernamesToIds", handlers.ApiHandlers.Users.TranslateUsernamesToIds),
            (r"/api/Users/GetById", handlers.ApiHandlers.Users.GetById),
            (r"/api/Users/GetFollows", handlers.ApiHandlers.Users.GetFollows),
            (r"/api/Users/CheckFollowsChannel", handlers.ApiHandlers.Users.CheckFollowsChannel),

            (r"/google(.*)", handlers.PageHandlers.Finders.Google),
            (r"/yandex(.*)", handlers.PageHandlers.Finders.Yandex),
            (r"/wmail(.*)", handlers.PageHandlers.Finders.Mail),
            (r"/sitemap(.*)", handlers.PageHandlers.Finders.Sitemap),
            (r"/robots.txt", handlers.PageHandlers.Finders.Robots),

            # Static
            (r"/static/", tornado.web.StaticFileHandler, dict(path=settings['static_path'])),

            # Index
            (r"/.*", handlers.PageHandlers.Twitch.Twitch)
        ], **settings, cookie_secret=cookie_secret)

    def __init__(self, ip: str, port: int):
        self.ip = ip
        self.port = port
        self.app = self.make_app()

    def start(self):
        logging.info("[Twitch.tornado.web] Web services starting on {}: {}".format(self.ip, self.port))
        print("[Twitch.tornado.web] Web services starting on {}: {}".format(self.ip, self.port))
        self.app.listen(self.port, self.ip)
