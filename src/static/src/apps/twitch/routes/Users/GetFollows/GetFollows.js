/* eslint-disable react/prop-types */
import React from 'react';
import { observer } from 'mobx-react';
import queryString from 'query-string';
import {
    Button, Form, FormControl, InputGroup
} from 'react-bootstrap';
import globalStore from '../../../../../stores/globalStore/globalStore';
import DefaultUsersPage from '../DefaultUsersPage';
import displayTypes from './DisplayTypes/displayTypes';
import displayTypesChecks from './DisplayTypes/displayTypesChecks';
import DisplayData from '../../../components/DisplayData/DisplayData';
import Pagi from '../../../components/Pagi/Pagi';
import FollowsCards from '../../../components/FollowsCards/FollowsCards';
import FollowsTable from '../../../components/FollowsTable/FollowsTable';
import localesService from '../../../locales/localesService';

const format = require(`string-format`);

const currentLocale = localesService.getCurrentLocale();
const routeLocale = localesService.getRouteLocale(currentLocale);
const pageLocale = localesService.getPageLocale(currentLocale);


@observer
class GetFollows extends DefaultUsersPage {
    constructor(props) {
        super(props);

        this.state = {
            displayType: displayTypes.cards
        };
    }

    onChangeLocation = async () => {
        const { userLogin, limit, offset } = queryString.parse(this.props.location.search);
        globalStore.setUserLogin(userLogin !== undefined ? userLogin : ``);
        globalStore.setLimit(limit !== undefined ? Number(limit) : 100);
        globalStore.setOffset(offset !== undefined ? Number(offset) : 0);

        await globalStore.getFollows();
    };

    handleSubmit = event => {
        const { userLogin, limit, offset } = globalStore;

        event && event.preventDefault();
        event && event.stopPropagation();
        if (!userLogin) {
            this.props.history.push(`${routeLocale.usersGetFollows.path}`);
            return;
        }

        this.props.history.push(`${routeLocale.usersGetFollows.path}?userLogin=${userLogin}&limit=${limit}&offset=${offset}`);
    };

    render() {
        const { displayType } = this.state;
        const {
            userLogin, setUserLogin, jsonDisplayData, isHideData, limit
        } = globalStore;

        const isHaveData = !!Object.values(jsonDisplayData).length;
        const maxPage = jsonDisplayData.total ? Math.floor(jsonDisplayData.total / limit) + 1 : 1;

        const followsCountText = format(pageLocale.usersGetFollows.followsCount, globalStore.userLogin, globalStore.jsonDisplayData.total || ``);
        return (
            <React.Fragment>
                <h1 className="h4">{pageLocale.usersGetFollows.title}</h1>
                <Form onSubmit={this.handleSubmit}>
                    <Form.Group>
                        <InputGroup>
                            <FormControl
                                type="text"
                                value={userLogin}
                                placeholder={pageLocale.usersGetFollows.inputPlaceholder}
                                onChange={(e) => setUserLogin(e.target.value)}
                            />
                            <InputGroup.Append>
                                <Button variant="primary" type="submit">
                                    <i className="fa fa-search" aria-hidden="true" />
                                </Button>
                            </InputGroup.Append>
                        </InputGroup>
                    </Form.Group>
                </Form>
                {displayTypesChecks({ displayType, setStateCheck: (item) => this.setState({ displayType: item }) })}

                {isHaveData
                    ? <h4>{followsCountText}</h4>
                    : null}
                {isHaveData
                    ? <Pagi handleSubmit={this.handleSubmit} maxPage={maxPage} />
                    : null}
                {displayType !== displayTypes.json
                    ? null
                    : <DisplayData jsonDisplayData={jsonDisplayData} isHideData={isHideData} />}
                {displayType !== displayTypes.cards
                    ? null
                    : <FollowsCards follows={jsonDisplayData.follows} />}
                {displayType !== displayTypes.table
                    ? null
                    : <FollowsTable follows={jsonDisplayData.follows} />}
                {isHaveData
                    ? <Pagi handleSubmit={this.handleSubmit} maxPage={maxPage} />
                    : null}
            </React.Fragment>
        );
    }
}


export default GetFollows;
