import { computed } from 'mobx';
import Actions from './Actions';

class Computed extends Actions {
    @computed get isHideData() {
        return !this.userLogin && !this.userId;
    }
}

export default Computed;
