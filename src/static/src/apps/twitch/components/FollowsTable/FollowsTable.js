import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Col, Row } from 'react-bootstrap';
import FollowRow from './FollowRow/FollowRow';


class FollowsTable extends React.Component {
    constructor(props) {
        super(props);

        this.state = {};
    }

    render() {
        const { follows } = this.props;
        if (!follows) {
            return null;
        }

        const usersRows = follows.map(item => (
            <Col key={item.channel.id} sx={12} sm={12} md={12} lg={12} xl={12}>
                <FollowRow key={item.channel.id} follow={item} />
            </Col>
        ));

        return (
            <Fragment>
                <Row>
                    {usersRows}
                </Row>
            </Fragment>
        );
    }
}

FollowsTable.propTypes = {
    follows: PropTypes.arrayOf(PropTypes.object)
};


export default FollowsTable;
