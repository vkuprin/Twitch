import { observable } from 'mobx';
import Computed from './Computed';

class GlobalStore extends Computed {
    @observable userLogin = ``;
    @observable userId = ``;
    @observable channelLogin = ``;
    @observable channelId = ``;
    @observable limit = 100;
    @observable offset = 0;
    @observable direction = `desc`;
    @observable sortBy = `created_at`;

    @observable jsonDisplayData = {};
}

export default new GlobalStore();
