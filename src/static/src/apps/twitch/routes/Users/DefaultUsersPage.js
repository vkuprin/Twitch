/* eslint-disable react/prop-types */
import React from 'react';
import { observer } from 'mobx-react';
import globalStore from '../../../../stores/globalStore/globalStore';


@observer
class DefaultUsersPage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {};
    }

    async componentDidMount() {
        globalStore.setJsonDisplayDataDefault();
        await this.onChangeLocation();
    }

    async componentDidUpdate(prevProps) {
        if (this.props.location !== prevProps.location) {
            await this.onChangeLocation();
        }
    }
}


export default DefaultUsersPage;
