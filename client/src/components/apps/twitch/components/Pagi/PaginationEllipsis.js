import React from 'react';
import styles from './styles.less';

class PaginationEllipsis extends React.Component {
    constructor(props) {
        super(props);

        this.state = {};
    }

    render() {
        return (
            <li>
                <span className={styles.paginationEllipsis}>…</span>
            </li>
        );
    }
}

export default PaginationEllipsis;
