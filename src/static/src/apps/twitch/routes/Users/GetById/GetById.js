/* eslint-disable react/prop-types */
import React from 'react';
import { observer } from 'mobx-react';
import queryString from 'query-string';
import {
    Button, Form, FormControl, InputGroup
} from 'react-bootstrap';
import globalStore from '../../../../../stores/globalStore/globalStore';
import DefaultUsersPage from '../DefaultUsersPage';
import displayTypes from './DisplayTypes/displayTypes';
import displayTypesChecks from './DisplayTypes/displayTypesChecks';
import DisplayData from '../../../components/DisplayData/DisplayData';
import UsersCards from '../../../components/UsersCards/UsersCards';
import localesService from '../../../locales/localesService';

const currentLocale = localesService.getCurrentLocale();
const routeLocale = localesService.getRouteLocale(currentLocale);

@observer
class GetById extends DefaultUsersPage {
    constructor(props) {
        super(props);

        this.state = {
            displayType: displayTypes.cards
        };
    }

    onChangeLocation = async () => {
        const { userId } = queryString.parse(this.props.location.search);
        globalStore.setUserId(userId !== undefined ? userId : ``);

        await globalStore.getById();
    };

    handleSubmit = event => {
        const { userId } = globalStore;

        event.preventDefault();
        event.stopPropagation();
        if (!userId) {
            this.props.history.push(`${routeLocale.usersGetById.path}`);
            return;
        }

        this.props.history.push(`${routeLocale.usersGetById.path}?userId=${userId}`);
    };

    render() {
        const { displayType } = this.state;
        const {
            userId, setUserId, jsonDisplayData, isHideData
        } = globalStore;

        return (
            <React.Fragment>
                <h1 className="h4">Get info about Twitch user by id</h1>
                <Form onSubmit={this.handleSubmit}>
                    <Form.Group>
                        <InputGroup>
                            <FormControl
                                type="text"
                                value={userId}
                                placeholder="Enter Twitch user id"
                                onChange={(e) => setUserId(e.target.value)}
                            />
                            <InputGroup.Append>
                                <Button variant="primary" type="submit">
                                    <i className="fa fa-search" aria-hidden="true" />
                                </Button>
                            </InputGroup.Append>
                        </InputGroup>
                    </Form.Group>
                </Form>
                {displayTypesChecks({ displayType, setStateCheck: (item) => this.setState({ displayType: item }) })}

                {displayType !== displayTypes.json
                    ? null
                    : <DisplayData jsonDisplayData={jsonDisplayData} isHideData={isHideData} />}
                {displayType !== displayTypes.cards
                    ? null
                    : <UsersCards users={jsonDisplayData.user ? [jsonDisplayData.user] : null} />}
            </React.Fragment>
        );
    }
}


export default GetById;
