from .server.PyServer import PyServer


async def main():
    server = PyServer(ip='127.0.0.1', port=8020)
    server.start()
