import _ from 'underscore';
import Twitch from './Twitch/Twitch';
import Users from './Users/Users';
import TranslateUsernamesToIds from './Users/TranslateUsernamesToIds/TranslateUsernamesToIds';
import GetById from './Users/GetById/GetById';
import GetFollows from './Users/GetFollows/GetFollows';
import CheckFollowsChannel from './Users/CheckFollowsChannel/CheckFollowsChannel';
import Test from './Test/Test';
import PageNotFound from './PageNotFound/PageNotFound';


const routes = {
    WoWnikTwitch: {
        uid: _.uniqueId(),
        exact: true,
        robots: `index, follow`,
        component: Twitch
    },
    users: {
        uid: _.uniqueId(),
        exact: true,
        robots: `index, follow`,
        component: Users
    },
    usersTranslateUsernamesToIds: {
        uid: _.uniqueId(),
        exact: true,
        robots: `index, follow`,
        component: TranslateUsernamesToIds
    },
    usersGetById: {
        uid: _.uniqueId(),
        exact: true,
        robots: `index, follow`,
        component: GetById
    },
    usersGetFollows: {
        uid: _.uniqueId(),
        exact: true,
        robots: `index, follow`,
        component: GetFollows
    },
    usersCheckFollowsChannel: {
        uid: _.uniqueId(),
        exact: true,
        robots: `index, follow`,
        component: CheckFollowsChannel
    },
    test: {
        uid: _.uniqueId(),
        exact: true,
        robots: `noindex, nofollow`,
        component: Test
    },
    pageNotFound: {
        uid: _.uniqueId(),
        exact: false,
        robots: `noindex, nofollow`,
        component: PageNotFound
    }
};


export default routes;
