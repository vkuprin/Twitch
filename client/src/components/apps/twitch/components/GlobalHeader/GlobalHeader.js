import React from 'react';
import { observer } from 'mobx-react';
import { LinkContainer } from 'react-router-bootstrap';
import {
    Container, Nav, Navbar, NavDropdown
} from 'react-bootstrap';
import styles from './styles.less';
import localesService from '../../locales/localesService';

const currentLocale = localesService.getCurrentLocale();
const routeLocale = localesService.getRouteLocale(currentLocale);
const headerLocale = localesService.getHeaderLocale(currentLocale);


@observer
class GlobalHeader extends React.Component {
    constructor(props) {
        super(props);

        this.state = {};
    }

    render() {
        return (
            <Navbar
                className={`navbar ${styles.header}`}
                collapseOnSelect
                expand="lg"
                bg="light"
                variant="light"
            >
                <Container>
                    <LinkContainer to={routeLocale.WoWnikTwitch.path}>
                        <Navbar.Brand>
                            {headerLocale.WoWnikTwitch.name}
                        </Navbar.Brand>
                    </LinkContainer>
                    <Navbar.Toggle />
                    <Navbar.Collapse>
                        <Nav className="mr-auto">
                            <NavDropdown title={headerLocale.users.name}>
                                <LinkContainer to={routeLocale.usersTranslateUsernamesToIds.path}>
                                    <NavDropdown.Item>
                                        {headerLocale.usersTranslateUsernamesToIds.name}
                                    </NavDropdown.Item>
                                </LinkContainer>
                                <LinkContainer to={routeLocale.usersGetById.path}>
                                    <NavDropdown.Item>
                                        {headerLocale.usersGetById.name}
                                    </NavDropdown.Item>
                                </LinkContainer>
                                <LinkContainer to={routeLocale.usersGetFollows.path}>
                                    <NavDropdown.Item>
                                        {headerLocale.usersGetFollows.name}
                                    </NavDropdown.Item>
                                </LinkContainer>
                                <LinkContainer to={routeLocale.usersCheckFollowsChannel.path}>
                                    <NavDropdown.Item>
                                        {headerLocale.usersCheckFollowsChannel.name}
                                    </NavDropdown.Item>
                                </LinkContainer>
                            </NavDropdown>
                            <NavDropdown title="Streams">
                                <LinkContainer to="/Streams/GetStreamByUser">
                                    <NavDropdown.Item>
                                        Get stream by user
                                    </NavDropdown.Item>
                                </LinkContainer>
                                <LinkContainer to="/Streams/GetLiveStreams">
                                    <NavDropdown.Item>
                                        Get live streams
                                    </NavDropdown.Item>
                                </LinkContainer>
                                <LinkContainer to="/Streams/GetSummary">
                                    <NavDropdown.Item>
                                        Get summary
                                    </NavDropdown.Item>
                                </LinkContainer>
                                <LinkContainer to="/Streams/GetFeatured">
                                    <NavDropdown.Item>
                                        Get featured
                                    </NavDropdown.Item>
                                </LinkContainer>
                                <LinkContainer to="/Streams/GetStreamsInCommunity">
                                    <NavDropdown.Item>
                                        Get streams in community
                                    </NavDropdown.Item>
                                </LinkContainer>
                            </NavDropdown>
                        </Nav>
                        <Nav>
                            <Nav.Link href="https://wownik.ru/PrivacyPolicy"> Privacy Policy </Nav.Link>
                            <NavDropdown title="GitLab">
                                <NavDropdown.Item href="https://gitlab.com/WoWnikCompany/Twitch">
                                    Site
                                </NavDropdown.Item>
                                <NavDropdown.Item href="https://gitlab.com/WoWnikCompany/TwitchExtension">
                                    Extension
                                </NavDropdown.Item>
                            </NavDropdown>
                            <Nav.Link href="https://wownik.ru">Info</Nav.Link>
                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
        );
    }
}


export default GlobalHeader;
