import React from 'react';
import { observer } from 'mobx-react';
import { ButtonGroup, Col, Row } from 'react-bootstrap';
import localesService from '../../locales/localesService';

const currentLocale = localesService.getCurrentLocale();
const pageLocale = localesService.getPageLocale(currentLocale);


@observer
class Twitch extends React.Component {
    constructor(props) {
        super(props);

        this.state = {};
    }

    render() {
        return (
            <Row className="justify-content-center text-center">
                <Col xs={12}>
                    <h1 className={`h5`}>
                        {pageLocale.WoWnikTwitch.extensionInfo}
                    </h1>
                </Col>
                <Col xs={12}>
                    <ButtonGroup vertical>
                        <a className="btn btn-primary" href="https://openuserjs.org/scripts/WoWnik/TwitchWoWnik">
                            Script
                        </a>
                        <a className="btn btn-primary" href="https://chrome.google.com/webstore/detail/eiflfidddkaideecknbbdonpkiddeegj">
                            Google Chrome Extension
                        </a>
                    </ButtonGroup>
                </Col>
            </Row>
        );
    }
}


export default Twitch;
