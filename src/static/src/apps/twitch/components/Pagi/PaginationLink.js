import React from 'react';
import PropTypes from 'prop-types';
import styles from './styles.less';

class PaginationLink extends React.Component {
    constructor(props) {
        super(props);

        this.state = {};
    }

    render() {
        const {
            text, active, onClick
        } = this.props;

        return (
            // eslint-disable-next-line jsx-a11y/no-noninteractive-element-interactions,jsx-a11y/click-events-have-key-events
            <li onClick={onClick}>
                <a className={active ? styles.paginationLinkActive : styles.paginationLink}>
                    {text}
                </a>
            </li>
        );
    }
}

PaginationLink.propTypes = {
    text: PropTypes.oneOfType(PropTypes.string, PropTypes.number),
    active: PropTypes.bool.isRequired,

    onClick: PropTypes.func
};
PaginationLink.defaultProps = {
    active: false
};

export default PaginationLink;
