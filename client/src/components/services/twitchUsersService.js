import FetchHelper from '../../../../FrontendCore/Helpers/FetchHelper';


const urls = {
    TranslateUsernamesToIds: `/api/Users/TranslateUsernamesToIds`,
    GetById: `/api/Users/GetById`,
    GetFollows: `/api/Users/GetFollows`,
    CheckFollowsChannel: `/api/Users/CheckFollowsChannel`
};

const twitchUsersService = {
    TranslateUsernamesToIds: ({ userLogin }) => FetchHelper.get_json(urls.TranslateUsernamesToIds, { userLogin }),
    GetById: ({ userId }) => FetchHelper.get_json(urls.GetById, { userId }),
    GetFollows: ({
        userId, limit, offset, direction, sortBy
    }) => FetchHelper.get_json(urls.GetFollows, {
        userId,
        limit,
        offset,
        direction,
        sortBy
    }),
    CheckFollowsChannel: ({ userId, channelId }) => FetchHelper.get_json(urls.CheckFollowsChannel, { userId, channelId })
};


export default twitchUsersService;
