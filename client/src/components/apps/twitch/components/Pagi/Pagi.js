/* eslint-disable react/prop-types */
import React from 'react';
import PropTypes from 'prop-types';
import { observer } from 'mobx-react';
import { Row } from 'react-bootstrap';
import globalStore from './../../../../stores/globalStore/globalStore';
import styles from './styles.less';
import PaginationEllipsis from './PaginationEllipsis';
import PaginationLink from './PaginationLink';


@observer
class Pagi extends React.Component {
    constructor(props) {
        super(props);

        this.state = {};
    }

    onClick = (page) => {
        const {
            limit, setOffset
        } = globalStore;

        setOffset(limit * (page - 1));
        this.props.handleSubmit();
    };

    render() {
        const {
            offset, limit
        } = globalStore;
        const { maxPage } = this.props;

        const currentPage = Math.floor(offset / limit) + 1;
        const maxLinkNumber = maxPage >= currentPage ? maxPage : currentPage;
        const displayLinksIndex = [1, currentPage - 1, currentPage, currentPage + 1, maxPage];
        const displayEllipsisIndex = [currentPage - 2, currentPage + 2];

        const links = [];
        for (let i = 1; i <= maxLinkNumber; i += 1) {
            if (displayLinksIndex.includes(i)) {
                links.push(<PaginationLink
                    key={i}
                    text={i}
                    onClick={() => this.onClick(i)}
                    active={i === currentPage}
                />);
            } else if (displayEllipsisIndex.includes(i)) {
                links.push(<PaginationEllipsis key={i} />);
            }
        }

        return (
            <React.Fragment>
                <Row className="justify-content-center text-center">
                    <nav className={styles.pagination}>
                        <ul className={styles.paginationList}>
                            <PaginationLink text={`<`} onClick={() => this.onClick(currentPage - 1)} />
                            {links}
                            <PaginationLink text={`>`} onClick={() => this.onClick(currentPage + 1)} />
                        </ul>
                    </nav>
                </Row>
            </React.Fragment>
        );
    }
}

Pagi.propTypes = {
    handleSubmit: PropTypes.func.isRequired,
    maxPage: PropTypes.number
};

Pagi.defaultProps = {
    maxPage: 1
};

export default Pagi;
