/* eslint-disable react/prop-types */
import React from 'react';
import { observer } from 'mobx-react';
import queryString from 'query-string';
import {
    Button, Form, FormControl, InputGroup
} from 'react-bootstrap';
import globalStore from '../../../../../stores/globalStore/globalStore';
import DefaultUsersPage from '../DefaultUsersPage';
import displayTypes from './DisplayTypes/displayTypes';
import displayTypesChecks from './DisplayTypes/displayTypesChecks';
import DisplayData from '../../../components/DisplayData/DisplayData';
import FollowsCards from '../../../components/FollowsCards/FollowsCards';
import FollowsTable from '../../../components/FollowsTable/FollowsTable';
import localesService from '../../../locales/localesService';

const currentLocale = localesService.getCurrentLocale();
const routeLocale = localesService.getRouteLocale(currentLocale);


@observer
class CheckFollowsChannel extends DefaultUsersPage {
    constructor(props) {
        super(props);

        this.state = {
            displayType: displayTypes.cards
        };
    }

    onChangeLocation = async () => {
        const { userLogin, channelLogin } = queryString.parse(this.props.location.search);
        globalStore.setUserLogin(userLogin !== undefined ? userLogin : ``);
        globalStore.setChannelLogin(channelLogin !== undefined ? channelLogin : ``);

        await globalStore.checkFollowsChannel();
    };

    handleSubmit = event => {
        const { userLogin, channelLogin } = globalStore;

        event.preventDefault();
        event.stopPropagation();
        if (!userLogin || !channelLogin) {
            this.props.history.push(`${routeLocale.usersCheckFollowsChannel.path}`);
            return;
        }

        this.props.history.push(`${routeLocale.usersCheckFollowsChannel.path}?userLogin=${userLogin}&channelLogin=${channelLogin}`);
    };

    render() {
        const { displayType } = this.state;
        const {
            userLogin, channelLogin, setUserLogin, setChannelLogin, jsonDisplayData, isHideData
        } = globalStore;

        return (
            <React.Fragment>
                <h1 className="h4">Get info about user follow to the Twitch channel</h1>
                <Form onSubmit={this.handleSubmit}>
                    <Form.Group>
                        <InputGroup>
                            <FormControl
                                type="text"
                                value={userLogin}
                                placeholder="Enter Twitch user login"
                                onChange={(e) => setUserLogin(e.target.value)}
                            />
                            <FormControl
                                type="text"
                                value={channelLogin}
                                placeholder="Enter Twitch channel login"
                                onChange={(e) => setChannelLogin(e.target.value)}
                            />
                            <InputGroup.Append>
                                <Button variant="primary" type="submit">
                                    <i className="fa fa-search" aria-hidden="true" />
                                </Button>
                            </InputGroup.Append>
                        </InputGroup>
                    </Form.Group>
                </Form>
                {displayTypesChecks({ displayType, setStateCheck: (item) => this.setState({ displayType: item }) })}

                {displayType !== displayTypes.json
                    ? null
                    : <DisplayData jsonDisplayData={jsonDisplayData} isHideData={isHideData} />}
                {displayType !== displayTypes.cards
                    ? null
                    : <FollowsCards follows={jsonDisplayData.follow ? [jsonDisplayData.follow] : null} />}
                {displayType !== displayTypes.table
                    ? null
                    : <FollowsTable follows={jsonDisplayData.follow ? [jsonDisplayData.follow] : null} />}
            </React.Fragment>
        );
    }
}


export default CheckFollowsChannel;
