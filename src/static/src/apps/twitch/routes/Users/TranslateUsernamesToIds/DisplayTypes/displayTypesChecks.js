import React from 'react';
import { Form } from 'react-bootstrap';
import displayTypes from './displayTypes';

const displayTypesChecks = (props) => {
    const { displayType } = props;

    return Object.entries(displayTypes).map(([key, item]) =>
        <Form.Check
            key={key}
            inline
            type="radio"
            label={key}
            checked={displayType === item}
            onChange={() => props.setStateCheck(item)}
        />);
};

export default displayTypesChecks;
