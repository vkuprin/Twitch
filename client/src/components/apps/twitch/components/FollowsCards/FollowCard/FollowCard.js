/* eslint-disable react/prop-types */
import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import {
    Button, Card, Col, Image, Modal, OverlayTrigger, Row, Tooltip
} from 'react-bootstrap';
import cn from 'classnames';
import JSONPretty from 'react-json-pretty';
import styles from './styles.less';


class FollowCard extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isModalOpen: false
        };
    }

    modalClose = () => {
        this.setState({ isModalOpen: false });
    };
    modalOpen = () => {
        this.setState({ isModalOpen: true });
    };

    render() {
        const { isModalOpen } = this.state;
        const { follow } = this.props;
        const { created_at, channel, notifications } = follow;

        return (
            <Fragment>
                <Card className={styles.cardSpace}>
                    {channel.profile_banner
                        ? <Card.Img
                            className={styles.cardBackImage}
                            variant="top"
                            src={channel.profile_banner}
                            alt={channel.display_name}
                        />
                        : <div className={cn(`card-img-top`, styles.cardBackImage)} />}
                    <Card.ImgOverlay className={styles.imgOverlay}>
                        <a
                            className={cn(styles.whiteLink)}
                            href={`https://www.twitch.tv/${channel.name}`}
                            target="_blank"
                            rel="noopener noreferrer"
                        >
                            <Row className="justify-content-center text-center">
                                <Col>
                                    <OverlayTrigger
                                        placement="top"
                                        delay={{ show: 100, hide: 100 }}
                                        overlay={<Tooltip>{channel.display_name}</Tooltip>}
                                    >
                                        <div>
                                            <Image
                                                className={styles.avatar}
                                                src={channel.logo}
                                                rounded
                                                alt={channel.display_name}
                                            />
                                            <h5 className={cn(styles.ellipsis)}>{channel.display_name}</h5>
                                        </div>
                                    </OverlayTrigger>
                                    <OverlayTrigger
                                        placement="top"
                                        delay={{ show: 100, hide: 100 }}
                                        overlay={<Tooltip
                                            style={{ display: channel.description ? `block` : `none` }}
                                        >{channel.description}</Tooltip>}
                                    >
                                        <div className={cn(styles.description, styles.ellipsis)}>
                                            {channel.description}
                                        </div>
                                    </OverlayTrigger>
                                </Col>
                            </Row>
                        </a>
                    </Card.ImgOverlay>
                    <Button variant="secondary" onClick={this.modalOpen}>
                        More info
                    </Button>
                </Card>

                <Modal show={isModalOpen} onHide={this.modalClose}>
                    <Modal.Header closeButton>
                        <Modal.Title>{channel.display_name}</Modal.Title>
                    </Modal.Header>

                    <Modal.Body>
                        <JSONPretty data={follow} />
                    </Modal.Body>

                    <Modal.Footer>
                        <Button variant="secondary" onClick={this.modalClose}>Close</Button>
                    </Modal.Footer>
                </Modal>
            </Fragment>
        );
    }
}

FollowCard.propTypes = {
    follow: PropTypes.shape({
        created_at: PropTypes.string,
        channel: PropTypes.shape({
            mature: PropTypes.bool,
            status: PropTypes.string,
            broadcaster_language: PropTypes.string,
            broadcaster_software: PropTypes.string,
            display_name: PropTypes.string,
            game: PropTypes.string,
            language: PropTypes.string,
            id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
            name: PropTypes.string,
            created_at: PropTypes.string,
            updated_at: PropTypes.string,
            partner: PropTypes.bool,
            logo: PropTypes.string,
            video_banner: PropTypes.string,
            profile_banner: PropTypes.string,
            profile_banner_background_color: PropTypes.string,
            url: PropTypes.string,
            views: PropTypes.number,
            followers: PropTypes.number,
            broadcaster_type: PropTypes.string,
            description: PropTypes.string,
            private_video: PropTypes.bool,
            privacy_options_enabled: PropTypes.bool
        }).isRequired,
        notifications: PropTypes.bool
    }).isRequired
};

export default FollowCard;
