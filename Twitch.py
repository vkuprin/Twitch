#!/usr/bin/python3.7

import asyncio
import logging

from src.Main import main

if __name__ == "__main__":
    logging.basicConfig(filename="file.log",
                        filemode="w", format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
                        level=logging.ERROR)

    loop = asyncio.get_event_loop()
    asyncio.ensure_future(main(), loop=loop)
    loop.run_forever()
