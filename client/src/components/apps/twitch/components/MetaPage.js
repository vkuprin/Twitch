/* eslint-disable react/prop-types */
import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { observer } from 'mobx-react';


@observer
class MetaPage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {};
    }

    componentDidMount() {
        this.updatePageInfo();
    }

    componentDidUpdate() {
        this.updatePageInfo();
    }

    updatePageInfo() {
        const { title, description, robots } = this.props;
        document.title = title;

        let descriptionMetaTag = document.querySelector(`meta[name=description]`);
        if (descriptionMetaTag === null) {
            descriptionMetaTag = document.createElement(`meta`);
            descriptionMetaTag.name = `description`;
            document.querySelector(`head`).append(descriptionMetaTag);
        }
        descriptionMetaTag.content = description;

        let robotsTag = document.querySelector(`meta[name=robots]`);
        if (robotsTag === null) {
            robotsTag = document.createElement(`meta`);
            robotsTag.name = `robots`;
            document.querySelector(`head`).append(robotsTag);
        }
        robotsTag.content = robots;
    }

    render() {
        return (
            <Fragment />
        );
    }
}

MetaPage.propTypes = {
    title: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    robots: PropTypes.string.isRequired
};


export default MetaPage;
