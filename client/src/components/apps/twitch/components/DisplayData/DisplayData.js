import React from 'react';
import PropTypes from 'prop-types';
import JSONPretty from 'react-json-pretty';


class DisplayData extends React.Component {
    constructor(props) {
        super(props);

        this.state = {};
    }

    render() {
        const { jsonDisplayData, isHideData } = this.props;

        return (
            <div>
                {isHideData ? null : <JSONPretty data={jsonDisplayData} />}
            </div>
        );
    }
}

DisplayData.propTypes = {
    jsonDisplayData: PropTypes.object.isRequired,
    isHideData: PropTypes.bool.isRequired
};


export default DisplayData;
