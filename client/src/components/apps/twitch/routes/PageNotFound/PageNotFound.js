import React from 'react';
import { observer } from 'mobx-react';
import { Col, Row } from 'react-bootstrap';
import localesService from '../../locales/localesService';

const currentLocale = localesService.getCurrentLocale();
const pageLocale = localesService.getPageLocale(currentLocale);


@observer
class PageNotFound extends React.Component {
    constructor(props) {
        super(props);

        this.state = {};
    }

    render() {
        return (
            <Row className="justify-content-center text-center">
                <Col md="auto">
                    <h1>404</h1>
                    <span>{pageLocale.pageNotFound.pageNotFoundText}</span>
                </Col>
            </Row>
        );
    }
}


export default PageNotFound;

