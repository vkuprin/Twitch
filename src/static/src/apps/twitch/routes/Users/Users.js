/* eslint-disable react/prop-types */
import React, { Fragment } from 'react';
import { observer } from 'mobx-react';
import localesService from '../../locales/localesService';

const currentLocale = localesService.getCurrentLocale();
const routeLocale = localesService.getRouteLocale(currentLocale);

@observer
class Users extends React.Component {
    constructor(props) {
        super(props);

        this.state = {};
    }

    componentDidMount() {
        this.props.history.replace(routeLocale.usersTranslateUsernamesToIds.path);
    }

    render() {
        return (
            <Fragment />
        );
    }
}


export default Users;
