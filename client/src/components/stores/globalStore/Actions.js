import { action } from 'mobx';
import globalLoaderAdapterV2 from '../../../../../FrontendCore/Components/globalLoaderV2/globalLoaderAdapterV2';
import { globalAlertAdapter } from '../../../../../FrontendCore/Components/globalAlert/globalAlertAdapter';
import twitchUsersService from '../../services/twitchUsersService';
import jsonDisplayDataDefault from '../../helpers/jsonDisplayDataDefault';

class Actions {
    @action setUserLogin = (userLogin) => {
        this.userLogin = userLogin;
    };
    @action setUserId = (userId) => {
        this.userId = userId;
    };
    @action setChannelLogin = (channelLogin) => {
        this.channelLogin = channelLogin;
    };
    @action setChannelId = (channelId) => {
        this.channelId = channelId;
    };
    @action setLimit = (limit) => {
        this.limit = limit;
    };
    @action setOffset = (offset) => {
        this.offset = offset;
    };
    @action setDirection = (direction) => {
        this.direction = direction;
    };
    @action setSortBy = (sortBy) => {
        this.sortBy = sortBy;
    };


    @action setJsonDisplayData = (data) => {
        this.jsonDisplayData = data;
    };
    @action setJsonDisplayDataDefault = () => {
        this.setJsonDisplayData(jsonDisplayDataDefault);
    };


    @action translateUsernamesToIds = async () => {
        const { userLogin } = this;
        const isValid = !!userLogin;
        if (!isValid) {
            return;
        }

        globalLoaderAdapterV2.addLoading();
        try {
            const jsonDisplayData = await twitchUsersService.TranslateUsernamesToIds({ userLogin });
            this.setJsonDisplayData(jsonDisplayData);
        } catch (e) {
            globalAlertAdapter.addDanger({ title: `Request error`, message: e.toString() });
        }
        globalLoaderAdapterV2.removeLoading();
    };
    @action getById = async () => {
        const { userId } = this;
        const isValid = !!userId;
        if (!isValid) {
            return;
        }

        globalLoaderAdapterV2.addLoading();
        try {
            const jsonDisplayData = await twitchUsersService.GetById({ userId });
            this.setJsonDisplayData(jsonDisplayData);
        } catch (e) {
            globalAlertAdapter.addDanger({ title: `Request error`, message: e.toString() });
        }
        globalLoaderAdapterV2.removeLoading();
    };
    @action getFollows = async () => {
        const {
            userLogin
        } = this;
        const isValid = !!userLogin;
        if (!isValid) {
            return;
        }

        globalLoaderAdapterV2.addLoading();
        try {
            const { users } = await twitchUsersService.TranslateUsernamesToIds({ userLogin });
            this.setUserId(users[0].id);

            const {
                userId, limit, offset, direction, sortBy
            } = this;
            const jsonDisplayData = await twitchUsersService.GetFollows({
                userId, limit, offset, direction, sortBy
            });
            this.setJsonDisplayData(jsonDisplayData);
        } catch (e) {
            globalAlertAdapter.addDanger({ title: `Request error`, message: e.toString() });
        }
        globalLoaderAdapterV2.removeLoading();
    };
    @action checkFollowsChannel = async () => {
        const { userLogin, channelLogin } = this;
        const isValid = !!(userLogin && channelLogin);
        if (!isValid) {
            return;
        }

        globalLoaderAdapterV2.addLoading();
        try {
            const { users } = await twitchUsersService.TranslateUsernamesToIds({ userLogin: [userLogin, channelLogin].join(`,`) });
            this.setUserId(users[0].id);
            this.setChannelId(users[1].id);

            const { userId, channelId } = this;
            const jsonDisplayData = await twitchUsersService.CheckFollowsChannel({ userId, channelId });
            this.setJsonDisplayData(jsonDisplayData);
        } catch (e) {
            globalAlertAdapter.addDanger({ title: `Request error`, message: e.toString() });
        }
        globalLoaderAdapterV2.removeLoading();
    };
}

export default Actions;
