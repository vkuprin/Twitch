import tornado.template
import tornado.web

from src.views import get_path


class BaseHandler(tornado.web.RequestHandler):
    @staticmethod
    def get_view(path: str) -> str:
        return get_path(path)

    @staticmethod
    def get_template(path: str) -> tornado.template.Template:
        return tornado.template.Template(open(BaseHandler.get_view(path)).read())

    @property
    def imports(self) -> bytes:
        return self.get_template("imports.html").generate(static_url=self.static_url)

    @property
    def google_anal(self) -> bytes:
        return self.get_template("google_anal.html").generate(static_url=self.static_url)

    def get_secured_cookie(
            self,
            name: str,
            value: str = None,
            max_age_days: int = 31,
            min_version: int = None
    ) -> str:
        res = super().get_secure_cookie(name, value, max_age_days, min_version)
        try:
            return res.decode("utf-8")
        except AttributeError:
            return value
