import json

from twitch.constants import DIRECTION_DESC, USERS_SORT_BY_CREATED_AT

from src.handlers.BaseHandlers import BaseHandler
from src.services import UsersService

users_service = UsersService()


class TranslateUsernamesToIds(BaseHandler):
    def get(self):
        user_login = self.get_argument("userLogin", "")

        users = users_service.translate_usernames_to_ids(user_login)

        data = {"users": users}
        self.write(json.dumps(data, default=str))


class GetById(BaseHandler):
    def get(self):
        user_id = self.get_argument("userId", None)

        if user_id is None:
            return

        user = users_service.get_by_id(user_id)

        data = {"user": user}
        self.write(json.dumps(data, default=str))


class GetFollows(BaseHandler):
    def get(self):
        user_id = self.get_argument("userId", None)
        limit = int(self.get_argument("limit", "25"))
        offset = int(self.get_argument("offset", "0"))
        direction = self.get_argument("direction", DIRECTION_DESC)
        sort_by = self.get_argument("sortBy", USERS_SORT_BY_CREATED_AT)
        if user_id is None:
            return

        data = users_service.get_follows(user_id, limit, offset, direction, sort_by)

        data = {
            "total": data.total,
            "follows": data.follows
        }
        self.write(json.dumps(data, default=str))


class CheckFollowsChannel(BaseHandler):
    def get(self):
        user_id = self.get_argument("userId", None)
        channel_id = self.get_argument("channelId", None)
        if user_id is None or channel_id is None:
            return

        follow = users_service.check_follows_channel(user_id, channel_id)

        data = {"follow": follow}
        self.write(json.dumps(data, default=str))
