/* eslint-disable react/prop-types */
import React from 'react';
import { withRouter } from 'react-router-dom';

const GA_TRACKING_ID = `UA-140896107-4`;

class GoogleAnalytics extends React.Component {
    // eslint-disable-next-line camelcase
    UNSAFE_componentWillUpdate({ location, history }) {
        const { gtag } = window;

        if (location.pathname === this.props.location.pathname) {
            // don't log identical link clicks (nav links likely)
            return;
        }

        if (history.action === `PUSH` &&
            typeof (gtag) === `function`) {
            gtag(`config`, GA_TRACKING_ID, {
                page_title: document.title,
                page_location: window.location.href,
                page_path: location.pathname
            });
        }
    }

    render() {
        return null;
    }
}

export default withRouter(GoogleAnalytics);
