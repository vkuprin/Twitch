from src.handlers.BaseHandlers import BaseHandler


class Google(BaseHandler):
    def get(self, s):
        self.render(self.get_view("google" + s))


class Yandex(BaseHandler):
    def get(self, s):
        self.render(self.get_view("yandex" + s))


class Mail(BaseHandler):
    def get(self, s):
        self.render(self.get_view("wmail" + s))


class Sitemap(BaseHandler):
    def get(self, s):
        self.render(self.get_view("sitemap" + s))


class Robots(BaseHandler):
    def get(self):
        self.render(self.get_view("robots.txt"))
