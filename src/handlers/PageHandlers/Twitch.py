from src.handlers.BaseHandlers import BaseHandler
from Submodules.Data import common


class Twitch(BaseHandler):
    def get(self):
        self.render(self.get_view("twitch.html"),
                    imports=self.imports,
                    google_anal=self.google_anal if common.is_prod else "")
