/* eslint-disable react/prop-types */
import React from 'react';
import { observer } from 'mobx-react';
import queryString from 'query-string';
import {
    Button, Form, FormControl, InputGroup
} from 'react-bootstrap';
import globalStore from '../../../../../stores/globalStore/globalStore';
import DefaultUsersPage from '../DefaultUsersPage';
import displayTypes from './DisplayTypes/displayTypes';
import displayTypesChecks from './DisplayTypes/displayTypesChecks';
import DisplayData from '../../../components/DisplayData/DisplayData';
import UsersCards from '../../../components/UsersCards/UsersCards';
import localesService from '../../../locales/localesService';

const currentLocale = localesService.getCurrentLocale();
const routeLocale = localesService.getRouteLocale(currentLocale);

@observer
class TranslateUsernamesToIds extends DefaultUsersPage {
    constructor(props) {
        super(props);

        this.state = {
            displayType: displayTypes.cards
        };
    }

    onChangeLocation = async () => {
        const { userLogin } = queryString.parse(this.props.location.search);
        globalStore.setUserLogin(userLogin !== undefined ? userLogin : ``);

        await globalStore.translateUsernamesToIds();
    };

    handleSubmit = event => {
        const { userLogin } = globalStore;

        event.preventDefault();
        event.stopPropagation();

        if (!userLogin) {
            this.props.history.push(`${routeLocale.usersTranslateUsernamesToIds.path}`);
            return;
        }

        this.props.history.push(`${routeLocale.usersTranslateUsernamesToIds.path}?userLogin=${userLogin}`);
    };

    render() {
        const { displayType } = this.state;
        const {
            userLogin, setUserLogin, jsonDisplayData, isHideData
        } = globalStore;

        return (
            <React.Fragment>
                <h1 className="h4">Get info about Twitch user by login</h1>
                <Form onSubmit={this.handleSubmit}>
                    <Form.Group>
                        <InputGroup>
                            <FormControl
                                type="text"
                                value={userLogin}
                                placeholder="Enter Twitch user login"
                                onChange={(e) => setUserLogin(e.target.value)}
                            />
                            <InputGroup.Append>
                                <Button variant="primary" type="submit">
                                    <i className="fa fa-search" aria-hidden="true" />
                                </Button>
                            </InputGroup.Append>
                        </InputGroup>
                    </Form.Group>
                </Form>
                {displayTypesChecks({ displayType, setStateCheck: (item) => this.setState({ displayType: item }) })}

                {displayType !== displayTypes.json
                    ? null
                    : <DisplayData jsonDisplayData={jsonDisplayData} isHideData={isHideData} />}
                {displayType !== displayTypes.cards
                    ? null
                    : <UsersCards users={jsonDisplayData.users} />}
            </React.Fragment>
        );
    }
}


export default TranslateUsernamesToIds;
