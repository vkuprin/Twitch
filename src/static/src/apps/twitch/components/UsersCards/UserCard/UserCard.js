/* eslint-disable react/prop-types */
import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import {
    Button, Card, Col, Image, Modal, OverlayTrigger, Row, Tooltip
} from 'react-bootstrap';
import cn from 'classnames';
import JSONPretty from 'react-json-pretty';
import styles from './styles.less';


class UserCard extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isModalOpen: false
        };
    }

    modalClose = () => {
        this.setState({ isModalOpen: false });
    };
    modalOpen = () => {
        this.setState({ isModalOpen: true });
    };

    render() {
        const { isModalOpen } = this.state;
        const { user } = this.props;

        return (
            <Fragment>
                <Card className={styles.cardSpace}>
                    {user.profile_banner
                        ? <Card.Img
                            className={styles.cardBackImage}
                            variant="top"
                            src={user.profile_banner}
                            alt={user.display_name}
                        />
                        : <div className={cn(`card-img-top`, styles.cardBackImage)} />}
                    <Card.ImgOverlay className={styles.imgOverlay}>
                        <a
                            className={cn(styles.whiteLink)}
                            href={`https://www.twitch.tv/${user.name}`}
                            target="_blank"
                            rel="noopener noreferrer"
                        >
                            <Row className="justify-content-center text-center">
                                <Col>
                                    <OverlayTrigger
                                        placement="top"
                                        delay={{ show: 100, hide: 100 }}
                                        overlay={<Tooltip>{user.display_name}</Tooltip>}
                                    >
                                        <div>
                                            <Image
                                                className={styles.avatar}
                                                src={user.logo}
                                                rounded
                                                alt={user.display_name}
                                            />
                                            <h5 className={cn(styles.ellipsis)}>{user.display_name}</h5>
                                        </div>
                                    </OverlayTrigger>
                                    <OverlayTrigger
                                        placement="top"
                                        delay={{ show: 100, hide: 100 }}
                                        overlay={<Tooltip
                                            style={{ display: user.bio ? `block` : `none` }}
                                        >{user.bio}</Tooltip>}
                                    >
                                        <div className={cn(styles.description, styles.ellipsis)}>
                                            {user.bio}
                                        </div>
                                    </OverlayTrigger>
                                </Col>
                            </Row>
                        </a>
                    </Card.ImgOverlay>
                    <Button variant="secondary" onClick={this.modalOpen}>
                        More info
                    </Button>
                </Card>

                <Modal show={isModalOpen} onHide={this.modalClose}>
                    <Modal.Header closeButton>
                        <Modal.Title>{user.display_name}</Modal.Title>
                    </Modal.Header>

                    <Modal.Body>
                        <JSONPretty data={user} />
                    </Modal.Body>

                    <Modal.Footer>
                        <Button variant="secondary" onClick={this.modalClose}>Close</Button>
                    </Modal.Footer>
                </Modal>
            </Fragment>
        );
    }
}

UserCard.propTypes = {
    user: PropTypes.shape({
        display_name: PropTypes.string,
        id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
        name: PropTypes.string,
        type: PropTypes.string,
        bio: PropTypes.string,
        created_at: PropTypes.string,
        updated_at: PropTypes.string,
        logo: PropTypes.string
    }).isRequired
};

export default UserCard;
