from typing import List

from twitch.resources import User

from pypattyrn.creational.singleton import Singleton
from Submodules.TwitchApiClient import TwitchApiClientV2
from Submodules.TwitchApiClient.python_twitch_client.resources import GetFollowsResponse

twitch_api_client = TwitchApiClientV2()


class UsersService(metaclass=Singleton):
    def translate_usernames_to_ids(self, twitch_user_login: str) -> List:
        twitch_user_login = twitch_user_login.replace(" ", "")
        data = twitch_api_client.users.translate_usernames_to_ids(twitch_user_login)
        return data

    def get_by_id(self, user_id: str) -> User:
        data = twitch_api_client.users.get_by_id(user_id)
        return data

    def get_follows(self, user_id, limit, offset, direction, sort_by) -> GetFollowsResponse:
        data = twitch_api_client.users.get_follows(user_id, limit, offset, direction, sort_by)
        return data

    def check_follows_channel(self, user_id, channel_id) -> List:
        data = twitch_api_client.users.check_follows_channel(user_id, channel_id)
        return data
