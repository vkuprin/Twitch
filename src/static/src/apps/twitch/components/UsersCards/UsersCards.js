import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Col, Row } from 'react-bootstrap';
import UserCard from './UserCard/UserCard';


class UsersCards extends React.Component {
    constructor(props) {
        super(props);

        this.state = {};
    }

    render() {
        const { users } = this.props;
        if (!users) {
            return null;
        }

        const usersCards = users.map(item => (
            <Col key={item.id} sx={12} sm={12} md={6} lg={4} xl={3}>
                <UserCard key={item.id} user={item} />
            </Col>
        ));

        return (
            <Fragment>
                <Row>
                    {usersCards}
                </Row>
            </Fragment>
        );
    }
}

UsersCards.propTypes = {
    users: PropTypes.arrayOf(PropTypes.object)
};


export default UsersCards;
