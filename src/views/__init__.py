import os

views = os.path.dirname(__file__)


def get_path(path: str) -> str:
    return views + "/" + path
