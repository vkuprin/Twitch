const displayTypes = {
    json: 0,
    cards: 1,
    table: 2
};

export default displayTypes;
